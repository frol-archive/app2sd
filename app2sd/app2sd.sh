#!/system/bin/sh
#
# App2sd for Android 2.1 (frol's variant)
#

install_srcdir="/sdcard/app2sd"
app2sd_partition="/dev/block/vold/179:2"
app2sd_mount_dir="/system/sdcard"
app2sd_dir="$app2sd_mount_dir/app2sd"
# target_dir is related to $app2sd_mount_dir
# Map some directories:
map_dirs="/data/app:app /data/dalvik-cache:dalvik-cache"	# /data/data:data /data/app-private:app-private
# Map whole data directory
#map_dirs="/data:data"
busybox="/system/xbin/busybox"
statefile="/system/etc/app2sd.state"

set -e

_remount_system() {
	mode="$1"
	system_partition=`mount | while read dev mnt junk; do case "$mnt" in /system) echo "$dev";; esac; done`
	mount -o remount,"$mode" -t yaffs2 "$system_partition" /system
}

app2sd_upgrade() {
	echo "APP2SD upgrade"
	echo ""

	if ! "$busybox" echo "" >/dev/null; then
		echo "Busybox is not installed" >&2
		exit 1
	fi

	echo "Remounting system partition read/write ..."
	_remount_system rw

	if "$busybox" [ -f "$statefile" ]; then
		. "$statefile"
	fi

	if "$busybox" [ "$APP2SD_INSTALLED" ]; then
		echo "APP2SD already installed. Upgrading."
	else
		echo "APP2SD is not installed, please run app2sd-install.sh"
		exit 1
	fi

	echo "Installing app2sd.sh ..."
	cat "$install_srcdir/app2sd.sh" >/system/bin/app2sd.sh
	chown root.root /system/bin/app2sd.sh
	chmod 0755 /system/bin/app2sd.sh

	echo "Installing new vold ..."
	if ! "$busybox" [ -f /system/bin/vold.orig ]; then
		mv /system/bin/vold /system/bin/vold.orig
	fi
	"busybox" rm /system/bin/vold.app2sd >/dev/null 2>&1 || :
	cat "$install_srcdir/vold" > /system/bin/vold.app2sd
	chown root.root /system/bin/vold.app2sd
	chmod 0755 /system/bin/vold.app2sd
	"busybox" rm /system/bin/vold >/dev/null 2>&1 || :
	ln -s /system/bin/vold.app2sd /system/bin/vold

	cat "$install_srcdir/vold.sh" > /system/etc/vold.sh
	chown root.root /system/etc/vold.sh
	chmod 0755 /system/etc/vold.sh

	echo "Remounting /system partition read-only ..."
	sync
	_remount_system ro || :

	echo "Done."
}

app2sd_install() {
	echo "APP2SD install for GT540"
	echo ""

	echo "Remounting system partition read/write ..."
	_remount_system rw

	if ! "$busybox" echo "" >/dev/null; then
		echo "Busybox is not installed" >&2
		exit 1
	fi

	if "$busybox" [ -f "$statefile" ]; then
		. "$statefile"
	fi

	if "$busybox" [ "$APP2SD_INSTALLED" ]; then
		echo "APP2SD already installed. Reinstalling."
	fi

	if "$busybox" [ -d "$app2sd_mount_dir" ]; then
		echo "Directory "$app2sd_mount_dir" already exists, skipping creation"
	else
		echo "Creating directory "$app2sd_mount_dir" ..."
		"$busybox" mkdir "$app2sd_mount_dir"
	fi

	echo "Mounting app2sd ext2 partition on "$app2sd_mount_dir" ..."

	# unmount partitions if they are mounted
	mount | while read dev mnt junk; do
		case "$dev" in
		"$app2sd_partition") umount "$mnt" >/dev/null 2>&1 || : ;;
		esac
	done
	umount "$app2sd_mount_dir" >/dev/null 2>&1 || :

	mount -t ext2 "$app2sd_partition" "$app2sd_mount_dir"

	"$busybox" [ -d "$app2sd_dir" ] || "$busybox" mkdir "$app2sd_dir"
	chown root.root "$app2sd_dir"
	chmod 0755 "$app2sd_dir"
	#echo "Clearing app2sd partition ..."
	#"busybox" rm -r "$app2sd_dir"/*

	echo "Installing app2sd.sh ..."
	cat "$install_srcdir/app2sd.sh" >/system/bin/app2sd.sh
	chown root.root /system/bin/app2sd.sh
	chmod 0755 /system/bin/app2sd.sh

	echo "Installing new vold ..."
	if ! "$busybox" [ -f /system/bin/vold.orig ]; then
		mv /system/bin/vold /system/bin/vold.orig
	fi
	"busybox" rm /system/bin/vold.app2sd >/dev/null 2>&1 || :
	cat "$install_srcdir/vold" > /system/bin/vold.app2sd
	chown root.root /system/bin/vold.app2sd
	chmod 0755 /system/bin/vold.app2sd
	"busybox" rm /system/bin/vold >/dev/null 2>&1 || :
	ln -s /system/bin/vold.app2sd /system/bin/vold

	cat "$install_srcdir/vold.sh" > /system/etc/vold.sh
	chown root.root /system/etc/vold.sh
	chmod 0755 /system/etc/vold.sh

	echo "Copying data directories ..."
	for pair in $map_dirs; do
		orig=`echo "$pair" | "$busybox" cut -d : -f 1`
		dest=`echo "$pair" | "$busybox" cut -d : -f 2`
		case "$orig" in
		/data/dalvik-cache)
			echo "  Clearing /data/dalvik-cache ..."
			"busybox" rm -f /data/dalvik-cache/*
			"busybox" rm -f "$app2sd_dir/$dest"/*
			;;
		esac
		echo "  Copying $orig to $app2sd_dir/$dest ..."
		"$busybox" mkdir -p "$app2sd_dir/$dest"
		"$busybox" tar cf - -C "$orig" . | "$busybox" tar xf - -C "$app2sd_dir/$dest"
	done

	echo "APP2SD_INSTALLED=1" > "$statefile"

	echo "Remounting /system partition read-only ..."
	sync
	_remount_system ro || :

	echo "Done. You need to reboot phone for changes to take effect"
}

app2sd_status() {
	echo "APP2SD status for GT540"
	echo ""

	if ! "$busybox" echo "" >/dev/null; then
		echo "Busybox is not installed" >&2
		exit 1
	fi

	if "$busybox" [ -f "$statefile" ]; then
		. "$statefile"
	fi

	if "$busybox" [ "$APP2SD_INSTALLED" ]; then
		echo "APP2SD is installed"
		echo "Partition usage:"
		df /data /system/sdcard | awk '{print "  " $1, $4, "/", $2, "used"}'
		echo "Mapped directories:"
		for pair in $map_dirs; do
			orig=`echo "$pair" | "$busybox" cut -d : -f 1`
			dest=`echo "$pair" | "$busybox" cut -d : -f 2`
			mounted=`mount | while read dev mnt junk; do case "$mnt" in "$orig") echo yes;; esac; done`
			if "$busybox" [ "$mounted" ]; then
				size=`"$busybox" du -ks "$app2sd_dir/$dest" 2>/dev/null | { read size junk; echo "${size}K"; }`
				echo "  $orig -> $app2sd_dir/$dest ($size used)"
			else
				echo "  $orig -> $app2sd_dir/$dest (not mounted)"
			fi
		done
	else
		echo "APP2SD is not installed"
	fi
}

app2sd_mount() {
	if "$busybox" echo "" >/dev/null && "$busybox" [ -f "$statefile" ]; then
		. "$statefile"
		if "$busybox" [ "$APP2SD_INSTALLED" ]; then
			mount -t ext2 "$app2sd_partition" "$app2sd_mount_dir"
			sleep 1

			for pair in $map_dirs; do
				orig=`echo "$pair" | "$busybox" cut -d : -f 1`
				dest=`echo "$pair" | "$busybox" cut -d : -f 2`
				mount -o bind "$app2sd_dir/$dest" "$orig"
			done
		fi
	fi
}

app2sd_umount() {
	if "$busybox" echo "" >/dev/null && "$busybox" [ -f "$statefile" ]; then
		. "$statefile"
		if "$busybox" [ "$APP2SD_INSTALLED" ]; then
			for pair in $map_dirs; do
				orig=`echo "$pair" | "$busybox" cut -d : -f 1`
				dest=`echo "$pair" | "$busybox" cut -d : -f 2`
				umount "$orig"
			done
			umount "$app2sd_mount_dir"
		fi
	fi
}

case "$1" in
install)
	app2sd_install
	;;
upgrade)
	app2sd_upgrade
	;;
status)
	app2sd_status
	;;
mount)
	app2sd_mount
	;;
umount)
	app2sd_umount
	;;
esac
