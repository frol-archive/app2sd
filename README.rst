Android 2.1 app2sd (frol's variant)
===================================

Requirements:
 * Android 2.1 (tested on LG GT540)
 * Busybox from Market

TODO:
 * Better directory selection. Move "data" and "app-private" directories to
   sdcard? Move individual directories from data?
 * Intelligent upgrade
 * More detailed status.

Installation
 * You need root;
 * Install Superuser, GScript Lite and Busybox from Market;
 * Run Busybox to install actually install busybox binary;
 * Copy app2sd and gscript directories to root of the sdcard;
 * Run app2sd-install.sh from GScript;
 * Reboot phone.
